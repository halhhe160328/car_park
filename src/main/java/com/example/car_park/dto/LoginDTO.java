package com.example.car_park.dto;

import lombok.Data;

@Data
public class LoginDTO {
    private String account;
    private String password;
}
