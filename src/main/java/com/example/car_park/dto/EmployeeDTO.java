package com.example.car_park.dto;


import com.example.car_park.entities.Roles;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Data
public class EmployeeDTO {
    private Long employeeId;
    private String account;
    private String department;
    private String employeeAddress;
    private LocalDate employeeBirth;
    private String employeeEmail;
    private String employeeName;
    private String employeePhone;
    private String password;
    private Integer sex;
    private String avatar;
    private boolean status;
    private LocalDateTime created;
    private Set<String> roles;
}
