package com.example.car_park.dto;

import lombok.Data;

import java.util.Set;

@Data
public class ParkingLotDTO {
    private Long parkId;
    private Long parkingArea;
    private String parkName;
    private String parkPlace;
    private Long parkPrice;
    private String parkStatus;
}
