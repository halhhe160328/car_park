package com.example.car_park.dto;


import lombok.Data;

import java.time.LocalTime;

@Data
public class TicketDTO {
    private Long ticketId;

    private LocalTime bookingTime;

    private String customerName;

    private String car;

    private Long tripTicket;
}
