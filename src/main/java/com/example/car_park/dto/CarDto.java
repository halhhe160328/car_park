package com.example.car_park.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CarDto {
    private String licensePlate;
    private String carType;
    private String carColor;
    private String company;
    private Long parkingLot;
}
