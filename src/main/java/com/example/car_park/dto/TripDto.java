package com.example.car_park.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TripDto {
    private Long tripId;
    private String destination;
    private LocalTime departureTime;
    private String driver;
    private String carType;
    private Integer maximumOnlineTicketNumber;
    private LocalDate departureDate;

}
