package com.example.car_park.dto;

import lombok.*;

import java.time.LocalDate;

@Data
public class BookingOfficeDto {
    private Long officeId;
    private String officeName;
    private Long tripBooking;
    private String officePhone;
    private String officePlace;
    private Integer officePrice;
    private LocalDate startContractDeadline;
    private LocalDate endContractDeadline;

}
