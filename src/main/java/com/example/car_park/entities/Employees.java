package com.example.car_park.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Table(name = "employee")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Employees {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employeeId;
    @Column(nullable = false)
    @Size(max = 50, message = "account<=50!")
    private String account;
    @Column(nullable = false)
    @Size(max = 30, message = "department<=30!")
    private String department;
    @Column
    private String employeeAddress;
    @Column
    private LocalDate employeeBirth;
    @Column
    private String employeeEmail;
    @Column(nullable = false)
    private String employeeName;
    @Column(nullable = false, unique = true)
    private String employeePhone;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private Integer sex;
    @Column
    private String avatar;
    @Column
    private boolean status;
    @Column
    private LocalDateTime created;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "employee_role",
            joinColumns = @JoinColumn(name = "employeeId"),
            inverseJoinColumns = @JoinColumn(name = "roleId"))
    private Set<Roles> roles = new HashSet<>();
}
