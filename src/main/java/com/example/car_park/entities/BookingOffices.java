package com.example.car_park.entities;

import jakarta.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Table(name = "bookingOffice")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BookingOffices {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long officeId;
    @Column
    private LocalDate endContractDeadline;
    @Column(nullable = false)
    private String officeName;
    @Column(nullable = false)
    private String officePhone;
    @Column
    private String officePlace;
    @Column(nullable = false)
    private Integer officePrice;
    @Column
    private LocalDate startContractDeadline;
    @ManyToOne
    @JoinColumn(name = "tripId")
    private Trips tripBooking;
}
