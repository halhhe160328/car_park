package com.example.car_park.entities;

import jakarta.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalTime;

@Table(name = "ticket")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Tickets {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ticketId;
    @Column
    private LocalTime bookingTime;
    @Column(nullable = false)
    private String customerName;
    @ManyToOne
    @JoinColumn(name = "licensePlate", nullable = false)
    private Cars car;
    @ManyToOne
    @JoinColumn(name = "tripId")
    private Trips tripTicket;

}
