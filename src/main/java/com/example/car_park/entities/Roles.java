package com.example.car_park.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;

@Entity
@Table(name = "role")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Roles {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long roleId;
    @Column
    @Enumerated(EnumType.STRING)
    private ERole roleName;


}
