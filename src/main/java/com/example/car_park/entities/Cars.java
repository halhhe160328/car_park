package com.example.car_park.entities;

import jakarta.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "car")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Cars {
    @Id
    @Column
    private String licensePlate;
    @Column(nullable = false)
    private String carColor;
    @Column
    private String carType;
    @Column
    private String company;
    @ManyToOne()
    @JoinColumn(name = "parkId")
    private ParkingLots parkingLot;
}
