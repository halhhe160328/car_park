package com.example.car_park.entities;

import jakarta.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Table(name = "parkinglot")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ParkingLots {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long parkId;
    @Column
    private Long parkingArea;
    @Column(nullable = false)
    private String parkName;
    @Column(nullable = false)
    private String parkPlace;
    @Column
    private Long parkPrice;
    @Column
    private String parkStatus;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE, mappedBy = "parkingLot")
    private Set<Cars> cars;


}
