package com.example.car_park.entities;

import jakarta.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;

@Table(name = "trip")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Trips {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long tripId;
    @Column
    private Integer bookedTicketNumber;
    @Column(nullable = false)
    private String carType;
    @Column
    private LocalDate departureDate;
    @Column(nullable = false)
    private LocalTime departureTime;
    @Column(nullable = false)
    private String destination;
    @Column(nullable = false)
    private String driver;
    @Column(nullable = false)
    private Integer maximumOnlineTicketNumber;
    @OneToMany(mappedBy = "tripTicket")
    private Set<Tickets> tickets;
    @OneToMany(mappedBy = "tripBooking")
    private Set<BookingOffices> bookingOffices;
}
