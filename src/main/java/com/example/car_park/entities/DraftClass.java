package com.example.car_park.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "draft")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DraftClass {
    @Id
    @Column
    private String licensePlate;
    @Column
    private LocalDate draftDate;
    @Column
    private LocalTime draftTime;
    private String year;
}
