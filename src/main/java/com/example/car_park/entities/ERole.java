package com.example.car_park.entities;

public enum ERole {
    ROLE_ADMIN, ROLE_MODERATOR, ROLE_USER;
}
