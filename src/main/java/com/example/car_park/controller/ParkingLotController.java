package com.example.car_park.controller;

import com.example.car_park.dto.ParkingLotDTO;
import com.example.car_park.service.ParkingLotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ParkingLotController {
    @Autowired
    ParkingLotService parkingLotService;

    @PostMapping("/addParkingLot")
    public ResponseEntity<Object> addParkingLot(@RequestBody ParkingLotDTO parkingLotDTO) {
        ParkingLotDTO parkingLotDTO1 = parkingLotService.addParking(parkingLotDTO);
        return new ResponseEntity<>(parkingLotDTO1, HttpStatus.CREATED);
    }

    @GetMapping("/listParkingLot")
    public ResponseEntity<Object> viewParkingLot() {
        List<ParkingLotDTO> parkingLotDTOS = parkingLotService.getAllParkingLot();
        return new ResponseEntity<>(parkingLotDTOS, HttpStatus.OK);
    }

    @PutMapping("/updateParkingLot/{id}")
    public ResponseEntity<Object> updateParkingLot(@PathVariable Long id, @RequestBody ParkingLotDTO parkingLotDTO) {
        ParkingLotDTO parkingLotDTO1 = parkingLotService.updateParkingLotById(id, parkingLotDTO);
        return new ResponseEntity<>(parkingLotDTO1, HttpStatus.OK);
    }

    @DeleteMapping("/deleteParkingLot/{id}")
    public ResponseEntity<Object> deleteParkingLot(@PathVariable Long id) {
        boolean deleted = parkingLotService.deleteParkingLotById(id);
        if (deleted) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
