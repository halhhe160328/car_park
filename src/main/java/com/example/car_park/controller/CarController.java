package com.example.car_park.controller;

import com.example.car_park.dto.CarDto;
import com.example.car_park.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {
    private CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping("/addCar")
    public ResponseEntity<Object> addCar(@RequestBody CarDto carDto) {
        CarDto carDto1 = carService.save(carDto);
        return new ResponseEntity<>(carDto1, HttpStatus.CREATED);
    }

    @GetMapping("/listCar")
    public ResponseEntity<Object> viewCar() {
        List<CarDto> viewCar = carService.findAll();
        return new ResponseEntity<>(viewCar, HttpStatus.OK);
    }

    @PutMapping("/updateCar/{licensePlate}")
    public ResponseEntity<Object> updateCar(@PathVariable String licensePlate, @RequestBody CarDto carDto) {
        CarDto carDto1 = carService.updateCar(licensePlate, carDto);
        return new ResponseEntity<>(carDto1, HttpStatus.OK);
    }

    @DeleteMapping("/deleteCar/{licensePlate}")
    public ResponseEntity<Object> deleteCar(@PathVariable String licensePlate) {
        boolean deleted = carService.delete(licensePlate);
        if (deleted) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
