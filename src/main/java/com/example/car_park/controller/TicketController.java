package com.example.car_park.controller;

import com.example.car_park.dto.TicketDTO;
import com.example.car_park.dto.TripDto;
import com.example.car_park.service.TicketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TicketController {
    private TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PostMapping("/addTicket")
    public ResponseEntity<Object> addTicket(@RequestBody TicketDTO ticketDTO) {
        TicketDTO ticketDTO1 = ticketService.addTicket(ticketDTO);
        return new ResponseEntity<>(ticketDTO1, HttpStatus.CREATED);
    }

    @GetMapping("/listTicket")
    public ResponseEntity<Object> viewTicket() {
        List<TicketDTO> viewTicket = ticketService.getAllTicket();
        return new ResponseEntity<>(viewTicket, HttpStatus.OK);
    }

    @DeleteMapping("/deleteTicket/{id}")
    public ResponseEntity<Object> deleteTicket(@PathVariable Long id) {
        boolean deleted = ticketService.deleteTicketById(id);
        if (deleted) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
