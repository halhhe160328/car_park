package com.example.car_park.controller;

import com.example.car_park.dto.LoginDTO;
import com.example.car_park.security.CustomEmployeeDetails;
import com.example.car_park.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "*")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider tokenProvider;

    @PostMapping("/signin")
    public ResponseEntity<String> loginUser(@RequestBody LoginDTO loginDTO) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getAccount(), loginDTO.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        CustomEmployeeDetails customEmployeeDetails = (CustomEmployeeDetails) authentication.getPrincipal();

        String jwt = tokenProvider.generateToken(customEmployeeDetails);

        return new ResponseEntity<>(jwt, HttpStatus.OK);
    }

}
