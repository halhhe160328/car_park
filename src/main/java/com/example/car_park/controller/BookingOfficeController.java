package com.example.car_park.controller;

import com.example.car_park.dto.BookingOfficeDto;
import com.example.car_park.service.BookingOfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController

public class BookingOfficeController {
    private BookingOfficeService bookingOfficeService;

    @Autowired
    public BookingOfficeController(BookingOfficeService bookingOfficeService) {
        this.bookingOfficeService = bookingOfficeService;
    }

    @PostMapping("/addBookingOffice")
    public ResponseEntity<Object> addBookingOffice(@RequestBody BookingOfficeDto bookingOfficeDto) {
        BookingOfficeDto bookingOfficeDto1 = bookingOfficeService.save(bookingOfficeDto);
        return new ResponseEntity<>(bookingOfficeDto1, HttpStatus.CREATED);
    }

    @GetMapping("/listBookingOffice")
    public ResponseEntity<Object> viewBookingOffice() {
        List<BookingOfficeDto> bookingOfficeDtoList = bookingOfficeService.findAll();
        return new ResponseEntity<>(bookingOfficeDtoList, HttpStatus.OK);
    }

}
