//package com.example.car_park.controller;
//
//import com.example.car_park.entities.ERole;
//import com.example.car_park.entities.Employees;
//import com.example.car_park.entities.Roles;
//import com.example.car_park.security.JwtTokenProvider;
//import com.example.car_park.payload.request.SigninRequest;
//import com.example.car_park.payload.response.JwtResponse;
//import com.example.car_park.payload.response.MessageResponse;
//import com.example.car_park.security.CustomEmployeeDetails;
//import com.example.car_park.service.EmployeeService;
//import com.example.car_park.service.RoleService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.web.bind.annotation.*;
//
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//@CrossOrigin
//@RestController
//@RequestMapping("/api/v1/auth")
//
//public class UserRestController {
//    @Autowired
//    private AuthenticationManager authenticationManager;
//    @Autowired
//    private JwtTokenProvider tokenProvider;
//    @Autowired
//    private EmployeeService employeeService;
//    @Autowired
//    private RoleService roleService;
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @PostMapping("/signup")
//    public ResponseEntity<?> register(@RequestBody SignupRequest signupRequest) {
//        if (employeeService.existsByAccount(signupRequest.getAccount())) {
//            return ResponseEntity.badRequest().body(new MessageResponse("error: account is already"));
//        }
//        if (employeeService.existsByEmployeeEmail(signupRequest.getEmployeeEmail())) {
//            return ResponseEntity.badRequest().body(new MessageResponse("error: email is already"));
//        }
//
//        Employees employee = new Employees();
//        employee.setAccount(signupRequest.getAccount());
//        employee.setDepartment(signupRequest.getDepartment());
//        employee.setEmployeeName(signupRequest.getEmployeeName());
//        employee.setEmployeeEmail(signupRequest.getEmployeeEmail());
//        employee.setEmployeePhone(signupRequest.getEmployeePhone());
//        employee.setPassword(passwordEncoder.encode(signupRequest.getPassword()));
//        employee.setSex(signupRequest.getSex());
//        employee.setCreated(LocalDateTime.now());
//        employee.setStatus(true);
//        Set<String> strRoles = signupRequest.getRoles();
//        Set<Roles> listRoles = new HashSet<>();
//
//        if (strRoles == null) {
//            //user quyen mac dinh
//            Roles userRole = roleService.findByRoleName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("not found user"));
//            listRoles.add(userRole);
//
//        } else {
//            strRoles.forEach(role -> {
//
//                switch (role) {
//                    case "ADMIN":
//                        Roles adminRole = roleService.findByRoleName(ERole.ROLE_ADMIN)
//                                .orElseThrow(() -> new RuntimeException("Admin role is not found"));
//                        listRoles.add(adminRole);
//
//                    case "MODERATOR":
//                        Roles modRole = roleService.findByRoleName(ERole.ROLE_MODERATOR)
//                                .orElseThrow(() -> new RuntimeException("mod role is not found"));
//                        listRoles.add(modRole);
//                    case "USER":
//                        Roles userRole = roleService.findByRoleName(ERole.ROLE_USER)
//                                .orElseThrow(() -> new RuntimeException("user role is not found"));
//                        listRoles.add(userRole);
//
//                }
//            });
//        }
//        employee.setRoles(listRoles);
//        employeeService.save(employee);
//        return ResponseEntity.ok(new MessageResponse("user registered successfully"));
//    }
//
//    @PostMapping("/signin")
//    public ResponseEntity<?> loginUser(@RequestBody SigninRequest signinRequest) {
//        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signinRequest.getAccount(), signinRequest.getPassword()));
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//
//        CustomEmployeeDetails customUserDetail = (CustomEmployeeDetails) authentication.getPrincipal();
//
//        String jwt = tokenProvider.generateToken(customUserDetail);
//
//        List<String> roles = new ArrayList<>();
//        for (GrantedAuthority authority : customUserDetail.getAuthorities()) {
//            roles.add(authority.getAuthority());
//        }
//        return ResponseEntity.ok(new JwtResponse(jwt, customUserDetail.getEmployeeId(), customUserDetail.getAccount(), customUserDetail.getEmployeePhone(), customUserDetail.getEmployeeEmail(), roles));
//    }
//}
