package com.example.car_park.controller;

import com.example.car_park.dto.EmployeeDTO;
import com.example.car_park.service.EmployeeService;
import com.example.car_park.service.FirebaseStorageService;
import com.example.car_park.utils.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/employee")
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;
    @Autowired
    FirebaseStorageService storageService;
    @Autowired
    Validate validate;
    @PostMapping("/addEmployee")
    public ResponseEntity<Object> addEmployee(@RequestBody EmployeeDTO employeeDTO) {
        if (!validate.checkExistAccountAndEmail(employeeDTO.getAccount(), employeeDTO.getEmployeeEmail())) {
            return new ResponseEntity<>("account or email is already", HttpStatus.NOT_ACCEPTABLE);
        }

        EmployeeDTO employeeDTO1 = employeeService.addEmployee(employeeDTO);
        return new ResponseEntity<>(employeeDTO1, HttpStatus.CREATED);
    }

    @GetMapping("/listEmployee")
    public ResponseEntity<Object> viewEmployee() {
        List<EmployeeDTO> employeeDTOS = employeeService.getAllEmployee();
        return new ResponseEntity<>(employeeDTOS, HttpStatus.OK);
    }

    @PutMapping("/updateEmployee/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable Long id, @RequestBody EmployeeDTO employeeDTO) {
        EmployeeDTO updateEmployee = employeeService.updateEmployeeById(id, employeeDTO);
        return new ResponseEntity<>(updateEmployee, HttpStatus.OK);
    }

    @DeleteMapping("/deleteEmployee/{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable Long id) {
        boolean deleted = employeeService.deleteEmployeeById(id);
        if (deleted) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/{userId}/uploadAvatar")
    public ResponseEntity<String> uploadAvatar(@PathVariable Long userId, @RequestParam("file") MultipartFile file) {
        try {
            // Lưu file vào Firebase Storage
            byte[] fileContent = file.getBytes();
            String avatarUrl = storageService.uploadFile(file.getOriginalFilename(), fileContent);
            EmployeeDTO employeeDTO = employeeService.findById(userId);
            if (employeeDTO != null) {
                employeeDTO.setAvatar(avatarUrl);
                employeeService.updateEmployeeById(userId, employeeDTO);
            }

            return ResponseEntity.ok("Avatar uploaded and user updated successfully!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Error uploading avatar!");
        }
    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity<InputStreamResource> downloadFile(@PathVariable String fileName) throws IOException {
        InputStream fileInputStream = storageService.downloadFile(fileName);

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.IMAGE_JPEG)
                .body(new InputStreamResource(fileInputStream));
    }
}
