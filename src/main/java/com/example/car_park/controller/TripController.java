package com.example.car_park.controller;

import com.example.car_park.dto.TripDto;
import com.example.car_park.service.BookingOfficeService;
import com.example.car_park.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class TripController {
    private TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PostMapping("/addTrip")
    public ResponseEntity<Object> addTrip(@RequestBody TripDto tripDto) {
        TripDto tripDto1 = tripService.save(tripDto);
        return new ResponseEntity<>(tripDto1, HttpStatus.CREATED);
    }

    @PutMapping("/updateTrip/{id}")
    public ResponseEntity<Object> updateTrip(@PathVariable Long id, @RequestBody TripDto tripDto) {
        TripDto updateTrip = tripService.updateTripById(id, tripDto);
        return new ResponseEntity<>(updateTrip, HttpStatus.OK);
    }

    @GetMapping("/listTrip")
    public ResponseEntity<Object> viewTrip() {
        List<TripDto> viewTrip = tripService.findAll();
        return new ResponseEntity<>(viewTrip, HttpStatus.OK);
    }

    @DeleteMapping("/deleteTrip/{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable Long id) {
        boolean deleted = tripService.delete(id);
        if (deleted) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
