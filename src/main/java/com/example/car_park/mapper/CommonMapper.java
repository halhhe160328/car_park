package com.example.car_park.mapper;

import org.modelmapper.ModelMapper;

public class CommonMapper {
    private static ModelMapper modelMapper = new ModelMapper();

    public static <T, U> T mapEntityToDto(U entity, Class<T> dtoClass) {
        return modelMapper.map(entity, dtoClass);
    }

    public static <T, U> U mapDtoToEntity(T dto, Class<U> entityClass) {
        return modelMapper.map(dto, entityClass);
    }}
