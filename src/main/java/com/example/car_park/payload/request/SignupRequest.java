//package com.example.car_park.payload.request;
//
//import lombok.Data;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//
//import java.time.LocalDateTime;
//import java.util.Set;
//@Getter
//@Setter
//@NoArgsConstructor
//public class SignupRequest {
//  private String account;
//  private String password;
//  private String employeeName;
//  private String employeeEmail;
//  private String employeePhone;
//  private String department;
//  private Integer sex;
//  private LocalDateTime created;
//  private boolean status;
//  private Set<String> roles;
//
//    public SignupRequest(String account, String password, String employeeName, String employeeEmail, String employeePhone, String department, Integer sex, LocalDateTime created, boolean status, Set<String> roles) {
//        this.account = account;
//        this.password = password;
//        this.employeeName = employeeName;
//        this.employeeEmail = employeeEmail;
//        this.employeePhone = employeePhone;
//        this.department = department;
//        this.sex = sex;
//        this.created = LocalDateTime.now();
//        this.status = true;
//        this.roles = roles;
//    }
//}
