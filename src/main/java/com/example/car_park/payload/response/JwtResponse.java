//package com.example.car_park.payload.response;
//
//import lombok.Data;
//
//import java.util.List;
//@Data
//public class JwtResponse {
//    private String token;
//    private String type="Bearer";
//    private Long employeeId;
//    private String account;
//    private String employeePhone;
//    private String employeeEmail;
//    private List<String> roles;
//
//    public JwtResponse(String token, Long employeeId, String account, String employeePhone, String employeeEmail, List<String> roles) {
//        this.token = token;
//        this.employeeId = employeeId;
//        this.account = account;
//        this.employeePhone = employeePhone;
//        this.employeeEmail = employeeEmail;
//        this.roles = roles;
//    }
//}
