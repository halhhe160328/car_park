package com.example.car_park.service;

import com.example.car_park.dto.CarDto;
import com.example.car_park.entities.Cars;
import com.example.car_park.entities.ParkingLots;
import com.example.car_park.mapper.CommonMapper;
import com.example.car_park.repository.CarRepository;
import com.example.car_park.repository.ParkingLotRepositories;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Repository
public class CarServiceImpl implements CarService {
    private CarRepository carRepository;
    private ParkingLotRepositories parkingLotRepositories;
    @Autowired
    public CarServiceImpl(CarRepository carRepository, ParkingLotRepositories parkingLotRepositories) {
        this.carRepository = carRepository;
        this.parkingLotRepositories = parkingLotRepositories;
    }

    @Override
    public CarDto save(CarDto carDto) {
        Cars car = CommonMapper.mapDtoToEntity(carDto, Cars.class);
        ParkingLots parkingLot = parkingLotRepositories.getReferenceById(carDto.getParkingLot());
        car.setParkingLot(parkingLot);
        carRepository.save(car);
        return carDto;
    }

    @Override
    public List<CarDto> findAll() {
        List<Cars> list = carRepository.findAll();
        List<CarDto> dtoList = new ArrayList<>();
        for (Cars car : list
        ) {
            CarDto carDto = CommonMapper.mapEntityToDto(car, CarDto.class);
            carDto.setParkingLot(car.getParkingLot().getParkId());
            dtoList.add(carDto);
        }
        return dtoList;
    }

    @Override
    public CarDto updateCar(String licensePlate, CarDto carDto) {
        try {
            Cars existingCar = carRepository.findById(licensePlate).orElse(null);
            if (existingCar != null) {
                if (carDto.getCarType() != null) {
                    existingCar.setCarType(carDto.getCarType());
                }
                if (carDto.getCarColor() != null) {
                    existingCar.setCarColor(carDto.getCarColor());
                }
                if (carDto.getCompany() != null) {
                    existingCar.setCompany(carDto.getCompany());
                }
                if (carDto.getParkingLot() != null) {
                    ParkingLots parkingLot = parkingLotRepositories.getReferenceById(carDto.getParkingLot());
                    existingCar.setParkingLot(parkingLot);
                }
                carRepository.save(existingCar);
                return carDto;
            }

        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public boolean delete(String licensePlate) {
        if (carRepository.existsById(licensePlate)) {
            carRepository.deleteById(licensePlate);
            return true;
        }
        return false;
    }
}
