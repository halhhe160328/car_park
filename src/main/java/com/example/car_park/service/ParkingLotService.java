package com.example.car_park.service;

import com.example.car_park.dto.EmployeeDTO;
import com.example.car_park.dto.ParkingLotDTO;

import java.util.ArrayList;
import java.util.List;

public interface ParkingLotService {
    ParkingLotDTO addParking(ParkingLotDTO parkingLotDTO);
    List<ParkingLotDTO> getAllParkingLot();
    ParkingLotDTO updateParkingLotById(Long id, ParkingLotDTO parkingLotDTO);
    boolean deleteParkingLotById(Long id);
}
