package com.example.car_park.service;

import com.example.car_park.dto.ParkingLotDTO;
import com.example.car_park.entities.ParkingLots;
import com.example.car_park.repository.ParkingLotRepositories;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParkingLotServiceImpl implements ParkingLotService {

    private ParkingLotRepositories parkingLotRepositories;

    @Autowired
    public ParkingLotServiceImpl(ParkingLotRepositories parkingLotRepositories) {
        this.parkingLotRepositories = parkingLotRepositories;
    }

    @Override
    public ParkingLotDTO addParking(ParkingLotDTO parkingLotDTO) {
        ModelMapper modelMapper = new ModelMapper();
        ParkingLots parkingLots = modelMapper.map(parkingLotDTO, ParkingLots.class);
        parkingLotRepositories.save(parkingLots);
        parkingLots.setParkId(parkingLots.getParkId());
        return parkingLotDTO;
    }

    @Override
    public List<ParkingLotDTO> getAllParkingLot() {
        ModelMapper modelMapper = new ModelMapper();
        List<ParkingLotDTO> parkingLotDTOS = new ArrayList<>();
        List<ParkingLots> parkingLotsList = parkingLotRepositories.findAll();
        for (ParkingLots parkingLots1 : parkingLotsList) {
            ParkingLotDTO parkingLotDTO = modelMapper.map(parkingLots1, ParkingLotDTO.class);
            parkingLotDTOS.add(parkingLotDTO);
        }
        return parkingLotDTOS;
    }

    @Override
    public ParkingLotDTO updateParkingLotById(Long id, ParkingLotDTO parkingLotDTO) {
        ParkingLots parkingLots = parkingLotRepositories.findById(id).orElse(null);
        if (parkingLots != null) {
            if (parkingLotDTO.getParkName() != null) {
                parkingLots.setParkName(parkingLotDTO.getParkName());
            }
            if (parkingLotDTO.getParkingArea() != null) {
                parkingLots.setParkingArea(parkingLotDTO.getParkingArea());
            }
            if (parkingLotDTO.getParkStatus() != null) {
                parkingLots.setParkStatus(parkingLotDTO.getParkStatus());
            }
            if (parkingLotDTO.getParkPlace() != null) {
                parkingLots.setParkPlace(parkingLotDTO.getParkPlace());
            }
            if (parkingLotDTO.getParkPrice() != null) {
                parkingLots.setParkPrice(parkingLotDTO.getParkPrice());
            }
            parkingLotRepositories.save(parkingLots);
            return parkingLotDTO;
        }
        return null;
    }

    @Override
    public boolean deleteParkingLotById(Long id) {
        if (parkingLotRepositories.existsById(id)) {
            parkingLotRepositories.deleteById(id);
            return true;
        }
        return false;
    }
}
