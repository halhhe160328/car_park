package com.example.car_park.service;

import com.example.car_park.dto.EmployeeDTO;
import com.example.car_park.entities.ERole;
import com.example.car_park.entities.Employees;
import com.example.car_park.entities.Roles;
import com.example.car_park.mapper.CommonMapper;
import com.example.car_park.repository.EmployeeRepositories;
import com.example.car_park.repository.RoleRepository;
import com.example.car_park.security.CustomEmployeeDetails;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Repository
public class EmployeeServiceImpl implements EmployeeService, UserDetailsService {

    private EmployeeRepositories employeeRepositories;
    private PasswordEncoder passwordEncoder;

    private RoleRepository roleRepository;

    public EmployeeServiceImpl(EmployeeRepositories employeeRepositories, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
        this.employeeRepositories = employeeRepositories;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @Override
    public EmployeeDTO addEmployee(EmployeeDTO employeeDTO) {
        Employees employee = new Employees();
        employee.setAccount(employeeDTO.getAccount());
        employee.setDepartment(employeeDTO.getDepartment());
        employee.setEmployeeName(employeeDTO.getEmployeeName());
        employee.setEmployeeEmail(employeeDTO.getEmployeeEmail());
        employee.setEmployeePhone(employeeDTO.getEmployeePhone());
        employee.setPassword(passwordEncoder.encode(employeeDTO.getPassword()));
        employee.setSex(employeeDTO.getSex());
        employee.setCreated(LocalDateTime.now());
        employee.setStatus(true);
        Set<String> strRoles = employeeDTO.getRoles();
        Set<Roles> listRoles = new HashSet<>();
        System.out.println("emprole"+ strRoles);
        if (strRoles == null) {
            //user quyen mac dinh
            Roles userRole = roleRepository.findByRoleName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("not found user"));
            listRoles.add(userRole);

        } else {
            strRoles.forEach(role -> {

                switch (role) {
                    case "ADMIN":
                        Roles adminRole = roleRepository.findByRoleName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Admin role is not found"));
                        listRoles.add(adminRole);
                        break;
                    case "MODERATOR":
                        Roles modRole = roleRepository.findByRoleName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("mod role is not found"));
                        listRoles.add(modRole);
                        break;
                    case "USER":
                        Roles userRole = roleRepository.findByRoleName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("user role is not found"));
                        listRoles.add(userRole);
                        break;

                }
            });
        }
        employee.setRoles(listRoles);
        employeeRepositories.save(employee);
        return employeeDTO;
    }

    @Override
    public List<EmployeeDTO> getAllEmployee() {
        ModelMapper modelMapper = new ModelMapper();
        List<EmployeeDTO> employeeDTOS = new ArrayList<>();
        List<Employees> employeesList = employeeRepositories.findAll();
        for (Employees employees1 : employeesList) {
            EmployeeDTO employeeDTO = modelMapper.map(employees1, EmployeeDTO.class);
            employeeDTOS.add(employeeDTO);
        }
        return employeeDTOS;
    }

    @Override
    public EmployeeDTO updateEmployeeById(Long id, EmployeeDTO employeeDTO) {
        Employees existingEmployee = employeeRepositories.findById(id).orElse(null);
        if (existingEmployee != null) {
            if (employeeDTO.getEmployeeName() != null) {
                existingEmployee.setEmployeeName(employeeDTO.getEmployeeName());
            }

            if (employeeDTO.getEmployeePhone() != null) {
                existingEmployee.setEmployeePhone(employeeDTO.getEmployeePhone());
            }

            if (employeeDTO.getEmployeeBirth() != null) {
                existingEmployee.setEmployeeBirth(employeeDTO.getEmployeeBirth());
            }

            if (employeeDTO.getSex() != null) {
                existingEmployee.setSex(employeeDTO.getSex());
            }

            if (employeeDTO.getEmployeeAddress() != null) {
                existingEmployee.setEmployeeAddress(employeeDTO.getEmployeeAddress());
            }

            if (employeeDTO.getEmployeeEmail() != null) {
                existingEmployee.setEmployeeEmail(employeeDTO.getEmployeeEmail());
            }

            if (employeeDTO.getAccount() != null) {
                existingEmployee.setAccount(employeeDTO.getAccount());
            }

            if (employeeDTO.getPassword() != null) {
                existingEmployee.setPassword(employeeDTO.getPassword());
            }
            if (employeeDTO.getAvatar() != null) {
                existingEmployee.setAvatar(employeeDTO.getAvatar());
            }
            if (employeeDTO.getDepartment() != null) {
                existingEmployee.setDepartment(employeeDTO.getDepartment());
            }
            if(employeeDTO.getRoles()!=null){
                Set<String> strRoles = employeeDTO.getRoles();
                Set<Roles> listRoles = new HashSet<>();
                System.out.println("emprole="+employeeDTO.getRoles());
                if (strRoles == null) {
                    //user quyen mac dinh
                    Roles userRole = roleRepository.findByRoleName(ERole.ROLE_USER).orElseThrow(() -> new RuntimeException("not found user"));
                    listRoles.add(userRole);

                } else {
                    strRoles.forEach(role -> {

                        switch (role) {
                            case "ADMIN":
                                Roles adminRole = roleRepository.findByRoleName(ERole.ROLE_ADMIN)
                                        .orElseThrow(() -> new RuntimeException("role is not found"));
                                listRoles.add(adminRole);

                            case "MODERATOR":
                                Roles modRole = roleRepository.findByRoleName(ERole.ROLE_MODERATOR)
                                        .orElseThrow(() -> new RuntimeException("role is not found"));
                                listRoles.add(modRole);
                            case "USER":
                                Roles userRole = roleRepository.findByRoleName(ERole.ROLE_USER)
                                        .orElseThrow(() -> new RuntimeException("role is not found"));
                                listRoles.add(userRole);

                        }
                    });
                }

            }

            employeeRepositories.save(existingEmployee);
            return employeeDTO;
        }
        return null;
    }

    @Override
    public boolean deleteEmployeeById(Long id) {
        if (employeeRepositories.existsById(id)) {
            employeeRepositories.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public EmployeeDTO findById(Long id) {
        Employees employees=employeeRepositories.findById(id).orElse(null);
        return CommonMapper.mapEntityToDto(employees,EmployeeDTO.class);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employees employees = employeeRepositories.findByAccount(username);
        if (employees == null) {
            throw new UsernameNotFoundException("not found employee");
        }
        return CustomEmployeeDetails.mapEmployeeToUserDetail(employees);
    }
}
