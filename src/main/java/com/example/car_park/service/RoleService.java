package com.example.car_park.service;

import com.example.car_park.entities.ERole;
import com.example.car_park.entities.Roles;

import java.util.Optional;

public interface RoleService {
    Optional<Roles> findByRoleName(ERole roleName);

}
