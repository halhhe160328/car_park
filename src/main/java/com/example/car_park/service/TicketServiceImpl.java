package com.example.car_park.service;

import com.example.car_park.dto.TicketDTO;
import com.example.car_park.entities.Cars;
import com.example.car_park.entities.Tickets;
import com.example.car_park.entities.Trips;
import com.example.car_park.repository.CarRepository;
import com.example.car_park.repository.TicketRepository;
import com.example.car_park.repository.TripRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Repository
public class TicketServiceImpl implements TicketService {
    private TicketRepository ticketRepository;
    private TripRepository tripRepository;
    private CarRepository carRepository;

    public TicketServiceImpl(TicketRepository ticketRepository, TripRepository tripRepository, CarRepository carRepository) {
        this.ticketRepository = ticketRepository;
        this.tripRepository = tripRepository;
        this.carRepository = carRepository;
    }

    @Override
    public TicketDTO addTicket(TicketDTO ticketDTO) {
        Tickets tickets = new ModelMapper().map(ticketDTO, Tickets.class);

        Trips trip = tripRepository.findById(ticketDTO.getTripTicket()).orElse(null);
        tickets.setTripTicket(trip);

        Cars car = carRepository.findById(ticketDTO.getCar()).orElse(null);
        tickets.setCar(car);

        ticketRepository.save(tickets);
        return ticketDTO;
    }

    @Override
    public List<TicketDTO> getAllTicket() {
        List<TicketDTO> ticketDTOList = new ArrayList<>();
        List<Tickets> ticketsList = ticketRepository.findAll();
        for (Tickets tickets : ticketsList) {
            TicketDTO ticketDTO = new TicketDTO();
            ticketDTO.setTicketId(tickets.getTicketId());
            ticketDTO.setTripTicket(tickets.getTripTicket().getTripId());
            ticketDTO.setCar(tickets.getCar().getLicensePlate());
            ticketDTO.setBookingTime(tickets.getBookingTime());
            ticketDTO.setCustomerName(tickets.getCustomerName());
            ticketDTOList.add(ticketDTO);
        }
        return ticketDTOList;
    }

    @Override
    public boolean deleteTicketById(Long id) {
        if(ticketRepository.existsById(id)){
            ticketRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
