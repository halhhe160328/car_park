package com.example.car_park.service;

import com.example.car_park.dto.BookingOfficeDto;

import java.util.List;

public interface BookingOfficeService {
    BookingOfficeDto save(BookingOfficeDto bookingOfficeDto);
    BookingOfficeDto getById(Long id);
    List<BookingOfficeDto> findAll();
}
