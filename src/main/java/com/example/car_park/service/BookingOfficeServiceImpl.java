package com.example.car_park.service;

import com.example.car_park.dto.BookingOfficeDto;
import com.example.car_park.entities.BookingOffices;
import com.example.car_park.entities.Trips;
import com.example.car_park.repository.BookingOfficeRepository;
import com.example.car_park.repository.TripRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Repository
public class BookingOfficeServiceImpl implements BookingOfficeService {
    private BookingOfficeRepository bookingOfficeRepository;
    private TripRepository tripRepository;
    @Autowired
    public BookingOfficeServiceImpl(BookingOfficeRepository bookingOfficeRepository, TripRepository tripRepository) {
        this.bookingOfficeRepository = bookingOfficeRepository;
        this.tripRepository = tripRepository;
    }

    @Override
    public BookingOfficeDto save(BookingOfficeDto bookingOfficeDto) {
        BookingOffices bookingOffice = new ModelMapper().map(bookingOfficeDto, BookingOffices.class);
        Trips trip = tripRepository.getById(bookingOfficeDto.getTripBooking());
        bookingOffice.setTripBooking(trip);
        bookingOfficeRepository.save(bookingOffice);
        return bookingOfficeDto;
    }



    @Override
    public BookingOfficeDto getById(Long id) {
        BookingOffices bookingOffice = bookingOfficeRepository.getById(id);
        return new ModelMapper().map(bookingOffice, BookingOfficeDto.class);
    }

    @Override
    public List<BookingOfficeDto> findAll() {
        List<BookingOffices> list = bookingOfficeRepository.findAll();
        List<BookingOfficeDto> bookingOfficeDtos = new ArrayList<>();
        for (BookingOffices bookingOffice : list) {
            BookingOfficeDto bookingOfficeDto = new ModelMapper().map(bookingOffice, BookingOfficeDto.class);
            bookingOfficeDtos.add(bookingOfficeDto);
        }
        return bookingOfficeDtos;
    }
}
