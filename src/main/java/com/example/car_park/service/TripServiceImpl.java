package com.example.car_park.service;

import com.example.car_park.dto.TripDto;
import com.example.car_park.entities.Trips;
import com.example.car_park.repository.TripRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@Repository
public class TripServiceImpl implements TripService {
    private TripRepository tripRepository;
    @Autowired
    public TripServiceImpl(TripRepository tripRepository) {
        this.tripRepository = tripRepository;
    }

    @Override
    public TripDto save(TripDto tripDto) {
        Trips trip = new ModelMapper().map(tripDto, Trips.class);
        tripRepository.save(trip);
        return tripDto;
    }

    @Override
    public boolean delete(Long id) {
        if (tripRepository.existsById(id)) {
            tripRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public TripDto getById(Long id) {
        return null;
    }

    @Override
    public List<TripDto> findAll() {
        List<TripDto> tripDtos = new ArrayList<>();
        List<Trips> list = tripRepository.findAll();
        for (Trips trip : list
        ) {
           TripDto tripDto=new ModelMapper().map(trip,TripDto.class);
           tripDtos.add(tripDto);
        }
        return tripDtos;
    }

    @Override
    public TripDto updateTripById(Long id, TripDto tripDto) {
        try {
            Trips existingTrip = tripRepository.findById(id).orElse(null);
            if (existingTrip != null) {
                if (tripDto.getCarType() != null) {
                    existingTrip.setCarType(tripDto.getCarType());
                }
                if (tripDto.getDestination() != null) {
                    existingTrip.setDestination(tripDto.getDestination());
                }
                if (tripDto.getDepartureTime() != null) {
                    existingTrip.setDepartureTime(tripDto.getDepartureTime());
                }
                if (tripDto.getDriver() != null) {
                    existingTrip.setDriver(tripDto.getDriver());
                }
                if (tripDto.getMaximumOnlineTicketNumber() != null) {
                    existingTrip.setMaximumOnlineTicketNumber(tripDto.getMaximumOnlineTicketNumber());
                }
                if (tripDto.getDepartureDate() != null) {
                    existingTrip.setDepartureDate(tripDto.getDepartureDate());
                }
                tripRepository.save(existingTrip);
                return tripDto;
            }
        } catch (Exception e) {
            throw new RuntimeException("update failed");
        }
        return null;
    }
}
