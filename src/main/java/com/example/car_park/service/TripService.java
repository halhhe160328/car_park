package com.example.car_park.service;

import com.example.car_park.dto.EmployeeDTO;
import com.example.car_park.dto.TripDto;

import java.util.List;

public interface TripService {
    TripDto save(TripDto tripDto);
    boolean delete(Long id);
    TripDto getById(Long id);
    List<TripDto> findAll();
    TripDto updateTripById(Long id, TripDto tripDto);
}
