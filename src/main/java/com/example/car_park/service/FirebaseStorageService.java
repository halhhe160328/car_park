package com.example.car_park.service;

import java.io.InputStream;

public interface FirebaseStorageService {
        String uploadFile(String fileName, byte[] fileContent);
        InputStream downloadFile(String fileName);


}
