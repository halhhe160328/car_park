package com.example.car_park.service;

import com.example.car_park.dto.EmployeeDTO;
import com.example.car_park.entities.Employees;

import java.util.ArrayList;
import java.util.List;

public interface EmployeeService {
     EmployeeDTO addEmployee(EmployeeDTO employeeDTO);
     List<EmployeeDTO> getAllEmployee();
     EmployeeDTO updateEmployeeById(Long id, EmployeeDTO employeeDTO);
     boolean deleteEmployeeById(Long id);
     EmployeeDTO findById(Long id);
}
