package com.example.car_park.service;

import com.example.car_park.dto.TicketDTO;

import java.util.List;

public interface TicketService {
    TicketDTO addTicket(TicketDTO ticketDTO);

    List<TicketDTO> getAllTicket();

    boolean deleteTicketById(Long id);
}
