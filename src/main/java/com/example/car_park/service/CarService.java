package com.example.car_park.service;

import com.example.car_park.dto.CarDto;

import java.util.List;

public interface CarService {
    CarDto save(CarDto carDto);
    List<CarDto> findAll();
    CarDto updateCar(String licensePlate,CarDto carDto);
    boolean delete(String licensePlate);
}
