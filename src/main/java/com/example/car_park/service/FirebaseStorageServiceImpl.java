package com.example.car_park.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.WriteChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

@Service
public class FirebaseStorageServiceImpl implements FirebaseStorageService {
    private final String bucketName = "carpark-e2c8f.appspot.com";

    @Override
    public String uploadFile(String fileName, byte[] fileContent) {
        try {
            Storage storage = StorageOptions.newBuilder()
                    .setCredentials(GoogleCredentials.fromStream(getClass().getResourceAsStream("/" +
                            "carpark-e2c8f-firebase-adminsdk-zgarx-1367235294.json")))
                    .build()
                    .getService();

            BlobInfo blobInfo = BlobInfo.newBuilder(bucketName, fileName).build();
            try (WriteChannel writer = storage.writer(blobInfo)) {
                writer.write(ByteBuffer.wrap(fileContent, 0, fileContent.length));
            }

            return "gs://" + bucketName + "/" + fileName;
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public InputStream downloadFile(String fileName) {
       try{
           Storage storage=StorageOptions.newBuilder()
                   .setCredentials(GoogleCredentials.fromStream(getClass().getResourceAsStream("/carpark-e2c8f-firebase-adminsdk-zgarx-1367235294.json")))
                   .build().getService();
           Blob blob = storage.get(bucketName, fileName);

           if (blob != null) {
               return Channels.newInputStream(blob.reader());
           } else {
               throw new RuntimeException("File not found in Firebase Storage");
           }
       } catch (IOException e) {
           throw new RuntimeException(e);
       }
    }
}
