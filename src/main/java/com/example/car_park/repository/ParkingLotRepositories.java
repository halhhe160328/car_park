package com.example.car_park.repository;

import com.example.car_park.entities.ParkingLots;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingLotRepositories extends JpaRepository<ParkingLots, Long> {
}
