package com.example.car_park.repository;

import com.example.car_park.entities.BookingOffices;
import com.example.car_park.entities.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingOfficeRepository extends JpaRepository<BookingOffices,Long> {
//    @Query( value="select b from BookingOffices b where b.officeId= :id")
//    BookingOffices getBookingOfficesById(@Param("Id") Long id);
}
