package com.example.car_park.repository;

import com.example.car_park.entities.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepositories extends JpaRepository<Employees, Long> {
    @Override
    Optional<Employees> findById(Long id);
    Optional<Employees> findByEmployeeEmail(String email);
    boolean existsByEmployeeEmail(String email);
    boolean existsByAccount(String account); //username
    Employees findByAccount(String account);
}
