package com.example.car_park.repository;

import com.example.car_park.entities.Trips;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripRepository extends JpaRepository<Trips,Long> {

}
