package com.example.car_park.repository;

import com.example.car_park.entities.ERole;
import com.example.car_park.entities.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Roles,Long> {
    Optional<Roles> findByRoleName(ERole roleName);

}
