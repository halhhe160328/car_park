package com.example.car_park.utils;

import com.example.car_park.repository.EmployeeRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class Validate {
    private EmployeeRepositories employeeRepositories;

    public Validate(EmployeeRepositories employeeRepositories) {
        this.employeeRepositories = employeeRepositories;
    }

    public boolean checkExistAccountAndEmail(String account, String email){
        if(!employeeRepositories.existsByAccount(account) && !employeeRepositories.existsByEmployeeEmail(email)){
           return true;
        }
        return false;
    }
}
