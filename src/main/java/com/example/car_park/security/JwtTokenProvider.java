package com.example.car_park.security;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;

@Component
@Slf4j
@Service
public class JwtTokenProvider {
    @Value("${ra.jwt.secret}")
    private String JWT_SECRET;

    @Value("${ra.jwt.expiration}")
    private Long JWT_EXPIRATION;

    public String generateToken(CustomEmployeeDetails customUserDetails) {
        Date now = new Date();
        Date dateExpire = new Date(now.getTime() + JWT_EXPIRATION);
        return Jwts.builder().
                setSubject(customUserDetails.getUsername())
                .setIssuedAt(now)
                .setExpiration(dateExpire)
                .signWith(SignatureAlgorithm.HS256, JWT_SECRET.getBytes())
                .compact();
    }

    public String getUserNameFromJwt(String token) {
        Claims claim = Jwts.parserBuilder()
                .setSigningKey(JWT_SECRET.getBytes())
                .build()
                .parseClaimsJwt(token)
                .getBody();
        return claim.getSubject();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET.getBytes())
                    .parseClaimsJwt(token);
            return true;
        } catch (MalformedJwtException ex) {
            log.error("invalid jwt token!");
        } catch (ExpiredJwtException ex) {
            log.error("expired jwt token");
        } catch (UnsupportedJwtException ex) {
            log.error("unsupported jwt token");
        } catch (IllegalArgumentException ex) {
            log.error("jwt claims string is empty");
        }
        return false;
    }
}
