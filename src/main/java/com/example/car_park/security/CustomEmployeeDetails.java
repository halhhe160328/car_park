package com.example.car_park.security;

import com.example.car_park.entities.Employees;
import com.example.car_park.entities.Roles;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomEmployeeDetails implements UserDetails{
    private Long employeeId;
    private String account;
    private String department;
    private String employeeEmail;
    private String employeeName;
    private String employeePhone;
    private String password;
    private Integer sex;
    private boolean status;
    private LocalDateTime created;
    private Collection<? extends GrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }
    public static CustomEmployeeDetails mapEmployeeToUserDetail(Employees employees) {
        List<GrantedAuthority> listAuthorities=new ArrayList<>();
        for (Roles role: employees.getRoles()
        ) {
            SimpleGrantedAuthority simpleGrantedAuthority=new SimpleGrantedAuthority(role.getRoleName().name());
            listAuthorities.add(simpleGrantedAuthority);
        }
        return new CustomEmployeeDetails(
                employees.getEmployeeId(),
                employees.getAccount(),
                employees.getDepartment(),
                employees.getEmployeeEmail(),
                employees.getEmployeeName(),
                employees.getEmployeePhone(),
                employees.getPassword(),
                employees.getSex(),
                employees.isStatus(),
                employees.getCreated(),
                listAuthorities
        );
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.account;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
