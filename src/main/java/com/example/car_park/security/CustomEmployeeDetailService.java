package com.example.car_park.security;

import com.example.car_park.entities.Employees;
import com.example.car_park.repository.EmployeeRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service

public class CustomEmployeeDetailService implements UserDetailsService {
    private EmployeeRepositories employeeRepositories;
    @Autowired
    public CustomEmployeeDetailService(EmployeeRepositories employeeRepositories) {
        this.employeeRepositories = employeeRepositories;
    }
    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        Employees user = employeeRepositories.findByAccount(account);
        if (user == null) {
            throw new UsernameNotFoundException("not found user");
        }
        return CustomEmployeeDetails.mapEmployeeToUserDetail(user);
    }
}
